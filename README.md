Learn how to make merge requests here

Before making any merge requests to an official project, use this space to test and learn how to make merge requests.

Yours sincerely.

marcelocripe

- - - - -

Aprenda a fazer os pedidos de mesclagem aqui

Antes de fazer qualquer pedido de mesclagem em um projeto oficial, utilize este espaço para testar e aprender a fazer os pedidos de mesclagem.

Atenciosamente.

marcelocripe
